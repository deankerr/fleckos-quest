import Phaser from 'phaser'

class GameScene extends Phaser.Scene
{
    constructor() {
      super({ key: 'gameScene'})
    }

    init() {
      this.gameW = this.sys.game.config.width;
      this.gameH = this.sys.game.config.height;
      this.cursors = this.input.keyboard.createCursorKeys()

      this.playerSpeed = 110
      this.enemyMinSpeed = 100
      this.enemyMaxSpeed = 200
      this.enemyMinY = 160
      this.enemyMaxY = 560

      this.playerStopped = false
      this.walkPlaying = false
    }

    create() {
        this.bg = this.add.sprite(0, 0, 'background').setOrigin(0).setScale(2)
        this.yell = []
        this.yell.push(this.sound.add('yell1'))
        this.yell.push(this.sound.add('yell2'))
        this.yell.push(this.sound.add('yell3'))
        this.yell.push(this.sound.add('yell4'))
        this.walk = this.sound.add('walk')

        this.winMusic = this.sound.add('funky')
        
        this.enemies = this.physics.add.group({
          key: 'dragon',
          repeat: 3,
          setXY: {
            x: 180,
            y: 300,
            stepX: 200,
            stepY: 100
          },
          immovable: true,
        });
        

        this.player = this.physics.add.sprite(60, 200, 'player').setScale(1).setImmovable()
        .setCollideWorldBounds()

        this.treasure = this.physics.add.sprite(1100, 400, 'treasure').setScale(1).setImmovable().setCollideWorldBounds()

        let enemies = this.enemies.getChildren()
        enemies[0].speed = this.rndDir() * 100
        enemies[1].speed = this.rndDir() * 150
        enemies[2].speed = this.rndDir() * 200
        enemies[3].speed = this.rndDir() * 150

        Phaser.Actions.Call(this.enemies.getChildren(), function(enemy){
          enemy.flipX = true;
          enemy.setVelocityY(enemy.speed)
        }, this);
        
        

        this.physics.add.overlap(this.player, this.enemies, this.playerDie, null, this)
        this.physics.add.overlap(this.player, this.treasure, this.win, null, this)
    }

    update() {
        // TESTING: restart/autowin
        //
        // if (this.cursors.space.isDown) {
        //   this.winMusic.stop()
        //   this.scene.restart()
        //   this.events.emit('playBgm')
        // }

        // if (this.cursors.shift.isDown) this.win()

        if (this.playerStopped) return

        if (this.cursors.left.isDown) {
          this.player.body.setVelocityX(-this.playerSpeed);
          this.player.setFlipX(true)
          this.playWalk()
        }
        else if (this.cursors.right.isDown) {
          this.player.body.setVelocityX(this.playerSpeed)
          this.player.setFlipX(false)
          this.playWalk()
        }
        else {
          this.player.body.setVelocityX(0)
        }

        if (this.cursors.up.isDown) {
          this.player.body.setVelocityY(-this.playerSpeed);
          this.playWalk()
        }
        else if (this.cursors.down.isDown) {
          this.player.body.setVelocityY(this.playerSpeed)
          this.playWalk()
        }
        else {
          this.player.body.setVelocityY(0)
        }

        if (!this.cursors.right.isDown && !this.cursors.left.isDown && !this.cursors.up.isDown && !this.cursors.down.isDown) {
          this.stopWalk()
        }

        this.enemies.getChildren().forEach(enemy => {
          if ((enemy.y <= this.enemyMinY && enemy.speed < 0) || (enemy.y >= this.enemyMaxY && enemy.speed > 0)) {
            enemy.speed *= -1
            enemy.setVelocityY(enemy.speed)
          }
          
        });

    }

    playWalk() {
      if (!this.walk.isPlaying) {
        this.walk.play({loop: true})
      }
    }

    stopWalk() {
      this.walk.stop()    
    }

    playerDie() {
      if (this.playerStopped) return

      this.playerStopped = true

      this.stopAll()

      this.yell[Math.floor(Math.random() * this.yell.length)].play()

      this.cameras.main.shake(500);
      
      this.cameras.main.on('camerashakecomplete', function(camera, effect){
        this.cameras.main.fade(500);
      }, this);

      this.cameras.main.on('camerafadeoutcomplete', function(camera, effect){
        this.scene.restart();
      }, this);
    }

    win() {
      if (this.playerStopped) return
      this.stopAll()

      this.treasure.setVisible(false)
      
      this.physics.world.disable(this.treasure)
      this.enemies.getChildren().forEach(enemy => {
        this.physics.world.disable(enemy)
      })
      
      let tweenTargets = this.enemies.getChildren()
      tweenTargets.push(this.bg)

      this.tweens.add({
        targets: tweenTargets,
        alpha: 0,
        duration: 2000,
        ease: 'Linear'
      })

      this.tweens.add({
        targets: this.player,
        x: this.gameW/2,
        y: this.gameH/2,
        scale: 4,
        duration: 2000,
        onComplete: this.playerDance,
        onCompleteScope: this
      })
    }

    playerDance() {
      this.events.emit('stopBgm')
      this.winMusic.play({loop: true})

      this.player.setFlipX(false)
      this.tweens.add({
        targets: this.player,
        x: this.gameW/2 - 350,
        duration: 1000,
        ease: 'Back',
        onComplete: this.playerDanceRight,
        onCompleteScope: this
      })

      const particles = this.add.particles('treasure')

      const emitter = particles.createEmitter({
        x: this.gameW/2,
        y: this.player.y + this.player.height * 4 - 20,
        speed: 600,
        scale: 2,
        angle: { min: 230, max: 310 },
        gravityY: 500,
        lifespan: 5000,
        follow: this.player,
        frequency: 250
      });

      this.time.addEvent({
        delay: 1000,
        repeat: 0,
        callbackScope: this,
        callback: this.finaleText
      })
    }

    finaleText() {
      let textBg = this.add.graphics();
      textBg.fillStyle(0x000000, 0.7);
      textBg.fillRect(100, 100, this.gameW - 200, this.gameH - 200);
      textBg.setAlpha(0)

      this.fadeIn(textBg)
      let textHeader = this.add.text(this.gameW/2, 200, 'Congratulations Flecko!', {
        fontFamily: 'Fondamento, Arial',
        fontSize: '60px',
        fill: '#ffffff',
      }).setOrigin(0.5, 0.5).setAlpha(0)
      this.fadeIn(textHeader)

      let textBody = [
        'Bully for you young Flecko! You have conquered every enemy you faced, including the most dangerous',
        'enemy of all - your fears. And laid claim to bountiful and endless treasures. For this you shall be',
        'rewarded, friend Flecko. A hearty greetings to you, valiant Flecko! We shall raise a warm mug of ale in',
        ' your honor and cry: "A toast to our young hero, Sir Flecko!" A warrior\'s feast of stuck pig, onions, and',
        'cheese awaits you upon your glorious return to the local hamlet.',
        'But take heart, plucky young Flecko, for your quest has only just begun...',
        '',
        'The End... ?'
      ]

      textBody.forEach((text, line) => {
        let textLine = this.add.text(this.gameW/2, 300 + line * 40, text,
          {
            fontFamily: 'Fondamento, Arial',
            fontSize: '22px',
            fill: '#ffffff',
          }).setOrigin(0.5, 0.5).setAlpha(0)
          this.fadeIn(textLine, 3000 + line * 1000)
      })
      this.fadeIn(textBody, 2000)
    }

    fadeIn(target, delay = 1000) {
      this.tweens.add({
        targets: target,
        alpha: 1,
        duration: 2000,
        ease: 'Linear',
        delay: delay
      })
    }

    playerDanceRight() {
      this.player.setFlipX(true)
      this.tweens.add({
        targets: this.player,
        x: this.gameW/2 + 350,
        duration: 1000,
        ease: 'Back',
        onComplete: this.playerDanceLeft,
        onCompleteScope: this
      })
    }

    playerDanceLeft() {
      this.player.setFlipX(false)
      this.tweens.add({
        targets: this.player,
        x: this.gameW/2 - 350,
        flipX: true,
        duration: 1000,
        ease: 'Back',
        onComplete: this.playerDanceRight,
        onCompleteScope: this
      })
    }
    
    stopAll() {
      this.playerStopped = true
      this.player.setVelocity(0)
      this.stopWalk()
      Phaser.Actions.Call(this.enemies.getChildren(), enemy => {
        enemy.setVelocityY(0)
      })
    }

    rndDir() {
      return Math.random() < 0.5 ? 1 : -1;
    }
}

export default GameScene
